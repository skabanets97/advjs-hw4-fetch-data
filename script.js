/*

Теоретичне питання
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX - это технология обращения к серверу без перезагрузки страницы.
За счёт этого уменьшается время отклика и веб-приложение по интерактивности
больше напоминает десктоп.

 */

import {getFilmsData, getFilmsCharacters} from "./requests.js";

window.onload = function () {
    document.body.classList.add('loaded_hiding');
    window.setTimeout(function () {
        document.body.classList.add('loaded');
        document.body.classList.remove('loaded_hiding');
    }, 1000);
}

const requestURL = 'https://ajax.test-danit.com/api/swapi/films';
const films = await getFilmsData(requestURL);

const renderFilms = films => {
    const listFilms = document.querySelector('.list-films');
    films.sort((a, b) => a.episodeId > b.episodeId ? 1 : -1);
    console.log(films);

    films.forEach(film => {
        let {episodeId, name, openingCrawl} = film;
        listFilms.innerHTML += `
        <div class="card-wrapper">
            <h1>Episod ${episodeId}</h1>
            <h2>Star Wars: ${name}</h2>
            <div class="description">
                <h3>Description:</h3>
                <p>${openingCrawl}</p>
            </div>
            <div class="characters"></div>
        </div>
    `
    })
}

renderFilms(films);

const renderCharacters = async films => {
    const charactersList = await getFilmsCharacters(films);

    charactersList.forEach( (characters, index) => {
        const charactersContainer = document.querySelectorAll('.characters');
        const charactersData = characters.map( character => `${character.name}`).join(',')

        charactersContainer[index].innerHTML += `
            <h3>Characters: </h3>
            <p>${charactersData}</p>
        `
    })

}

renderCharacters(films);












