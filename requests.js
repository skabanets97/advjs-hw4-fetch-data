export const getFilmsData = async url => {
    try {
        const response = await fetch(url)

        if(response.ok) {
            const result = await response.json()
            return result
        } else {
            throw new Error('The request failed!')
        }

    } catch(err) {
        console.error(err);
    }
};

export const getFilmsCharacters = async films => {
    films.sort((a, b) => a.episodeId > b.episodeId ? 1 : -1);
    const charactersData = films.map(film => Promise.all(film.characters.map( url => getFilmsData(url))))
    const charactersItems = await Promise.all(charactersData)
    console.log(charactersItems)
    return charactersItems
}

